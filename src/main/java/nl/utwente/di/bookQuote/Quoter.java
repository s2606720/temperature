package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private HashMap<String, Double> map = new HashMap<String, Double>();

    public Quoter() {
        this.map.put("1", 10.0);
        this.map.put("2", 45.0);
        this.map.put("3", 20.0);
        this.map.put("4", 35.0);
        this.map.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        if(Integer.parseInt(isbn) > 0 && Integer.parseInt(isbn) < 6) {
            return map.get(isbn);
        } else return 0.0;
    }

    public double toFahrenheit(String celsius) {
        int c = Integer.parseInt(celsius);
        return (c * (9.0/5.0)) + 32;
    }
}
